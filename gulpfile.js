const gulp = require('gulp');
const watch = require('gulp-watch');
const browserSync = require('browser-sync').create();


gulp.task('watch', function(done) {
    browserSync.init({
        browser: "firefox developer edition",
        proxy: 'http://localhost:8888/flexbox-tutorial-real-examples/',
        port:8886
    });
    gulp.watch('index.html').on('change', browserSync.reload);
    gulp.watch('style.css').on('change', browserSync.reload);
});